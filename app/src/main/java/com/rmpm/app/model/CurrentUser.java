package com.rmpm.app.model;


import org.json.JSONObject;

import java.util.concurrent.CopyOnWriteArrayList;


public class CurrentUser extends BaseModel {

    private String id;
    private String fullName;
    private String empId;
    private String mobile;
    private String email;
    private String role;
    private String uid;
    private String plantId;
    private String unitId;
    private String deviceType;
    private String deviceToken;
    private String authorization;
    private CopyOnWriteArrayList<Material> materialList;
    private CopyOnWriteArrayList<MaterialAdmin> adminMaterialList;

    public CurrentUser(JSONObject jsonResponse) {
        this.id = getValue(jsonResponse, kId, String.class);
        this.fullName = getValue(jsonResponse, kFullName, String.class);
        this.empId = getValue(jsonResponse, kEmpID, String.class);
        this.mobile = getValue(jsonResponse, kMobile, String.class);
        this.email = getValue(jsonResponse, kEmailId, String.class);
        this.role = getValue(jsonResponse, kRole, String.class);
        this.uid = getValue(jsonResponse, kUId, String.class);
        this.plantId = getValue(jsonResponse, kPlantId, String.class);
        this.unitId = getValue(jsonResponse, kUnitId, String.class);
       // this.deviceType = getValue(jsonResponse, kDeviceType, String.class);
        //this.deviceToken = getValue(jsonResponse, kDeviceToken, String.class);
        this.authorization = getValue(jsonResponse, kAuthorization, String.class);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public void setMaterialList(CopyOnWriteArrayList<Material> copyOnWriteArrayList) {
        this.materialList = copyOnWriteArrayList;
    }

    public CopyOnWriteArrayList<Material> getMaterialList() {
        return materialList;
    }


    public CopyOnWriteArrayList<MaterialAdmin> getAdminMaterialList() {
        return adminMaterialList;
    }

    public void setAdminMaterialList(CopyOnWriteArrayList<MaterialAdmin> adminMaterialList) {
        this.adminMaterialList = adminMaterialList;
    }

}
