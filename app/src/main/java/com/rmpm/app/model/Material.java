package com.rmpm.app.model;

import com.google.gson.JsonObject;
import com.rmpm.app.constants.Constants;

import org.json.JSONObject;

public class Material extends BaseModel {

    private String Id;
    private String material;
    private String material_type_id;
    private String line_id;
    private String status;
    private String arrival_in;
    private String updated_date;
    private String plant_id;
    private String unit_id;
    private String uid;
    private String initialq;
    private String bufferstockday;
    private String constantvalue;
    private String last_value;
    private String last_insert;


    public Material(JSONObject jsonResponse) {
        this.Id = getValue(jsonResponse, kID, String.class);
        this.material = getValue(jsonResponse, kMaterial, String.class);
        this.material_type_id = getValue(jsonResponse, kMaterialTypeID, String.class);
        this.line_id = getValue(jsonResponse,kLineId , String.class);
        this.status = getValue(jsonResponse, kStatus, String.class);
        this.arrival_in = getValue(jsonResponse, kArrivalIn, String.class);
       // this.updated_date = getValue(jsonResponse, kUpdateDate, String.class);
        this.plant_id = getValue(jsonResponse, kPlantID, String.class);
        this.unit_id = getValue(jsonResponse, kUnitID, String.class);
        this.uid = getValue(jsonResponse, kUId, String.class);
        this.initialq = getValue(jsonResponse, kIntiialq, String.class);
        this.bufferstockday = getValue(jsonResponse, kBufferStockday, String.class);
       // this.constantvalue = getValue(jsonResponse, kConstantValue, String.class);
        this.last_value = getValue(jsonResponse, kLastValue, String.class);
        this.last_insert = getValue(jsonResponse, kLastInsert, String.class);
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterial_type_id() {
        return material_type_id;
    }

    public void setMaterial_type_id(String material_type_id) {
        this.material_type_id = material_type_id;
    }

    public String getLine_id() {
        return line_id;
    }

    public void setLine_id(String line_id) {
        this.line_id = line_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getArrival_in() {
        return arrival_in;
    }

    public void setArrival_in(String arrival_in) {
        this.arrival_in = arrival_in;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public String getPlant_id() {
        return plant_id;
    }

    public void setPlant_id(String plant_id) {
        this.plant_id = plant_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getInitialq() {
        return initialq;
    }

    public void setInitialq(String initialq) {
        this.initialq = initialq;
    }

    public String getBufferstockday() {
        return bufferstockday;
    }

    public void setBufferstockday(String bufferstockday) {
        this.bufferstockday = bufferstockday;
    }

    public String getConstantvalue() {
        return constantvalue;
    }

    public void setConstantvalue(String constantvalue) {
        this.constantvalue = constantvalue;
    }

    public String getLast_value() {
        return last_value;
    }

    public void setLast_value(String last_value) {
        this.last_value = last_value;
    }

    public String getLast_insert() {
        return last_insert;
    }

    public void setLast_insert(String last_insert) {
        this.last_insert = last_insert;
    }
}
