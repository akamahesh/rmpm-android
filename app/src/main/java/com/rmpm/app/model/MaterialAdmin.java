package com.rmpm.app.model;

import com.rmpm.app.constants.Constants;

import org.json.JSONObject;

public class MaterialAdmin extends BaseModel {

    private String Id;
    private String materialType;
    private String materialCode;
    private String status;
    private String arrivalIn;
    private String updatedDate;
    private String plantId;
    private String unitId;
    private String uid;
    private String total;

    public MaterialAdmin(JSONObject jsonResponse) {
        this.Id = getValue(jsonResponse, kID, String.class);
        this.materialType = getValue(jsonResponse, Constants.kMaterialType, String.class);
        this.materialCode = getValue(jsonResponse, kMaterialCode, String.class);
        this.status = getValue(jsonResponse, kStatus, String.class);
        this.arrivalIn = getValue(jsonResponse, kArrivalIn, String.class);
        this.plantId = getValue(jsonResponse, kPlantID, String.class);
        this.unitId = getValue(jsonResponse, kUnitID, String.class);
        this.uid = getValue(jsonResponse, kUId, String.class);
        this.total = getValue(jsonResponse, Constants.kTotal, String.class);
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getArrivalIn() {
        return arrivalIn;
    }

    public void setArrivalIn(String arrivalIn) {
        this.arrivalIn = arrivalIn;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}

