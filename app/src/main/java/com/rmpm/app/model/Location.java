package com.rmpm.app.model;


import org.json.JSONObject;


public class Location extends BaseModel {

    private String lineId;
    private String lineType;
    private String status;
    private String section;
    private String plantId;
    private String unitId;
    private String uid;

    public Location(JSONObject jsonResponse) {
        this.lineId = getValue(jsonResponse, kLineId, String.class);
        this.lineType = getValue(jsonResponse, kLineType, String.class);
        this.status = getValue(jsonResponse, kStatus, String.class);
        this.section = getValue(jsonResponse, kStatus, String.class);
        this.plantId = getValue(jsonResponse, kPlantId, String.class);
        this.unitId = getValue(jsonResponse, kUnitId, String.class);
        this.uid = getValue(jsonResponse, kUId, String.class);
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
