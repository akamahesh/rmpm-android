package com.rmpm.app.constants;

import java.util.Objects;

/**
 * To hold all the constants keys and enums throughout the app.
 */
public interface Constants {

    //TAGS
    String kTagAccounts = "RMPMAccountLogs ==> ";
    String kTagAPI = "RMPMAPILogs ==> ";
    String kTagTracking = "RMPMTracking ==> ";


    //common constants used
    String kEmptyString = "";
    Integer kEmptyNumber = 0;
    String kFrom = "from";
    String kData = "data";
    String kType = "type";
    String kIsAlreadyOpened = "opened";
    String kAccessToken = "access_token";

    //api key constants
    String kUserName = "username";
    String kPassword = "password";
    String kRoot = "Root";
    String kId = "Id";
    String kFullName = "message";
    String kEmpID = "empid";
    String kMobile = "mobile";
    String kEmailId = "emailid";
    String kArrivalIn = "arrival_in";
    String kRole = "role";
    String kUserIP = "user_ip";
    String kUId = "uid";
    String kPlantId = "plant_id";
    String kUnitId = "unit_id";
    String kotp_value = "otp_value";
    String kUserFailedLogins = "user_failed_logins";
    String kUserLastFailedLogins = "user_last_failed_login";
    String kDeviceType = "deviceType";
    String kDeviceToken = "deviceToken";
    String kAuthorization = "Authorization";

    //Location listing
    String kLineId = "line_id";
    String kLineType = "line_type";
    String kSection = "line_id";

    String kID = "Id";
    String kMaterial ="material";
    String kTotal ="total";
    String kMaterialType ="material_type";
    String kMaterialCode ="material_code";
    String kMaterialTypeID ="material_type_id";
    String kUpdateDate ="updated_date";
    String kPlantID ="plant_id";
    String kUnitID ="unit_id";
    String kIntiialq ="initialq";
    String kBufferStockday ="bufferstockday";
    String kConstantValue ="constantvalue";
    String kLastValue ="last_value";
    String kLastInsert ="last_insert";

    String kDefaultAppName = "RMPM";
    String kAppPreferences = kDefaultAppName+"AppPreferences";

    String kStatus = "status";
    String kMessage = "message";
    String kCurrentUser = "currentUser";

    String kMessageServerNotRespondingError = kDefaultAppName + " server not responding!";
    String kMessageInternalInconsistency = "Some internal inconsistency occurred. Please try again.";
    String kMessageNetworkError = "Device does not connect to internet.";
    String kSocketTimeOut = kDefaultAppName + " Server not responding..";

    //facebook constants

    String kFacebookFirstName = "first_name";
    String kFacebookLastName = "last_name";
    String kFacebookGender = "gender";
    String kFacebookFields = "fields";
    String kFacebookAllFields = "id,name,link,email,picture,first_name,last_name,gender,friends";
    String kFacebookEmail = "email";
    String kFacebookPublicProfile = "public_profile";


    /**
     * enum to define From where the LoginActivity was initiated.
     */
    enum LOGIN_FROM {
        login,
        registration,
        verification,
        autheticator
    }


    /**
     * Status Enumeration for Task Status
     */
    enum Status {
        success(0),
        fail(1),
        reachLimit(2),
        noChange(3),
        history(4),
        normal(5),
        discard(6);

        private int value;

        Status(int status) {
            this.value = status;
        }

        public static Status getStatus(int value) {
            for (Status status : Status.values()) {
                if (status.value == value) {
                    return status;
                }
            }
            return fail;
        }

        /**
         * To get Integer value of corresponding enum
         */
        public Integer getValue() {
            return this.value;
        }
    }


    /**
     * Http Status for Api Response
     */
    enum HTTPStatus {
        success("success"),
        failure("FAILURE"),
        invalid("invalid"),
        error("ERROR");

        private String httpStatus;

        HTTPStatus(String httpStatus) {
            this.httpStatus = httpStatus;
        }

        public static HTTPStatus getStatus(String status) {
            for (HTTPStatus httpStatus : HTTPStatus.values()) {
                if (Objects.equals(httpStatus.httpStatus, status)) {
                    return httpStatus;
                }
            }
            return error;
        }

        public String getValue() {
            return this.httpStatus;
        }


    }
}


