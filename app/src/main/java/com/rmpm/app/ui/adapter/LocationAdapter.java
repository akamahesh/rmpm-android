package com.rmpm.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.model.Location;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> implements Filterable {

    private Context context;
    private List<Location> mList;
    private List<Location> locationListFiltered;

    private LocationAdapterInterface mListener;
    private boolean isAdmin;

    public LocationAdapter(Context context, List<Location> mList, LocationAdapterInterface locationAdapterInterface, boolean isAdmin) {
        this.mList = mList;
        this.context = context;
        this.mListener = locationAdapterInterface;
        this.isAdmin = isAdmin;
        this.locationListFiltered =mList;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_location_item, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        Location location = mList.get(position);
        holder.bindContent(location);
        holder.tvTitle.setText(location.getLineType());
        if (isAdmin) {
            if (position % 2 == 0)
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.green));
            else
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.red));
        }

    }


    @Override
    public int getItemCount() {
        return locationListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    locationListFiltered = mList;
                } else {
                    List<Location> filteredList = new ArrayList<>();
                    for (Location row : mList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getLineType().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    locationListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = locationListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                locationListFiltered = (ArrayList<Location>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface LocationAdapterInterface {
        void onItemSelected(Location item);
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private Location location;

        LocationViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            itemView.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onItemSelected(location);
                }
            });

        }

        void bindContent(Location item) {
            this.location = item;
        }

    }
}
