package com.rmpm.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.ui.adapter.InventoryAdapter;

import static com.rmpm.app.ui.activity.AdminHomeActivity.IS_ADMIN;

public class ResultActivity extends AppCompatActivity {

    boolean isAdmin;
    InventoryAdapter.InventoryAdapterInterface inventoryAdapterInterface = new InventoryAdapter.InventoryAdapterInterface() {
        @Override
        public void onItemSelected(String item) {

        }
    };

    public static Intent getIntent(Context context, boolean isAdmin) {
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra(IS_ADMIN, isAdmin);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        isAdmin = getIntent().getBooleanExtra(IS_ADMIN, false);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Material Profile");
        ImageView ivBack = findViewById(R.id.ivBack);
        ImageView ivRight = findViewById(R.id.ivRight);
        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });
        ivRight.setVisibility(View.VISIBLE);
        ivRight.setOnClickListener(v -> {
            //  onBackPressed();
        });

        Utils.hideKeyboard(this);


    }

}
