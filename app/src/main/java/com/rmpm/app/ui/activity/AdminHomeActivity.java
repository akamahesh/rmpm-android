package com.rmpm.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.ui.fragments.AdminMaterialFragment;
import com.rmpm.app.ui.fragments.LocationFragment;
import com.rmpm.app.ui.fragments.MaterialFragment;

import java.util.ArrayList;
import java.util.List;

public class AdminHomeActivity extends AppCompatActivity {
    public static String IS_ADMIN = "isAdmin";

    private boolean isAdmin;
    public static Intent getIntent(Context context, boolean isAdmin) {
        Intent intent = new Intent(context, AdminHomeActivity.class);
        intent.putExtra(IS_ADMIN,isAdmin);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        isAdmin = getIntent().getBooleanExtra(IS_ADMIN,false);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Inventory Report");
        ImageView ivBack = findViewById(R.id.ivBack);
        ImageView ivLogout = findViewById(R.id.ivLogout);
        ivLogout.setVisibility(View.VISIBLE);
        ivLogout.setOnClickListener(v->{
            ModelManager.modelManager().setCurrentUser(null);
            finishAffinity();
            startActivity(LoginActivity.getIntent(AdminHomeActivity.this));
        });
        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        Utils.hideKeyboard(this);

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(LocationFragment.newInstance(isAdmin), "Location");
        adapter.addFragment(AdminMaterialFragment.newInstance(isAdmin, null), "Material");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
