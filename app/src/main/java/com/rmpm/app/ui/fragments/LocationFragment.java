package com.rmpm.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.rmpm.app.R;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;
import com.rmpm.app.helper.DividerItemRecyclerDecoration;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.managers.ReachabilityManager;
import com.rmpm.app.model.Location;
import com.rmpm.app.ui.activity.InventoryActivity;
import com.rmpm.app.ui.activity.UserHomeActivity;
import com.rmpm.app.ui.adapter.LocationAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.rmpm.app.ui.activity.AdminHomeActivity.IS_ADMIN;


public class LocationFragment extends Fragment {

    LocationAdapter locationAdapter;
    String TAG = getClass().getName();
    private boolean isAdmin;
    private List<Location> mNewsFeedList;
    LocationAdapter.LocationAdapterInterface locationAdapterInterface = new LocationAdapter.LocationAdapterInterface() {
        @Override
        public void onItemSelected(Location item) {
            if (isAdmin)
                startActivity(InventoryActivity.getIntent(getContext(), isAdmin,item));
            else
                startActivity(InventoryActivity.getIntent(getContext(), isAdmin,item));

            //((UserHomeActivity) Objects.requireNonNull(getActivity())).gotoMaterial(item);
        }
    };

    private SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            //filter recycler view when query submit
            locationAdapter.getFilter().filter(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            locationAdapter.getFilter().filter(newText);
            return false;
        }
    };
    public LocationFragment() {
        // Required empty public constructor
    }

    public static LocationFragment newInstance(boolean isAdmin) {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_ADMIN, isAdmin);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isAdmin = getArguments().getBoolean(IS_ADMIN);
        }
        mNewsFeedList = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        SearchView searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(onQueryTextListener);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        locationAdapter = new LocationAdapter(getContext(), mNewsFeedList, locationAdapterInterface, isAdmin);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        recyclerView.setAdapter(locationAdapter);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(isAdmin){
            getLocation();
        }else{
            getLocation();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isAdmin)
            ((UserHomeActivity) Objects.requireNonNull(getActivity())).updateToolbarTitle("Location");
    }

    private void getLocation() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(getActivity(), "Location List Failed!", "Sorry, Failed to reach RMPM servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        ModelManager.modelManager().getLocations((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Location>> genricResponse) -> {
            showProgress(false);
            CopyOnWriteArrayList<Location> locations = genricResponse.getObject();
            Log.v(TAG, "Response : " + locations.size());
            mNewsFeedList.clear();
            mNewsFeedList.addAll(locations);
            locationAdapter.notifyDataSetChanged();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(getActivity(), "Process Failed!", message);
        });

    }

    private void showProgress(boolean b) {
    }


}
