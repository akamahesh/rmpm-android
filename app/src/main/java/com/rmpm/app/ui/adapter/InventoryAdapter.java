package com.rmpm.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.model.Material;

import java.util.List;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.InventoryViewHolder> {

    private Context context;
    private List<Material> mList;
    private boolean isAdmin;
    private InventoryAdapterInterface mListener;

    public InventoryAdapter(Context context, List<Material> mList, InventoryAdapterInterface locationAdapterInterface, boolean isAdmin) {
        this.mList = mList;
        this.isAdmin = isAdmin;
        this.context = context;
        this.mListener = locationAdapterInterface;
    }

    @NonNull
    @Override
    public InventoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inventory_item, parent, false);
        return new InventoryViewHolder(view,isAdmin);
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryViewHolder holder, int position) {
        String title = mList.get(position).getMaterial();
        holder.bindContent(title);
        holder.tvTitle.setText(title);
        if (isAdmin) {
            if (position % 2 == 0)
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.green));
            else
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.red));

        }

    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface InventoryAdapterInterface {
        void onItemSelected(String item);
    }

    class InventoryViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private EditText edtAvailable;
        private EditText edtRequired;
        private String item;


        public InventoryViewHolder(View itemView,boolean isAdmin) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            edtAvailable = itemView.findViewById(R.id.edtAvailable);
            edtRequired = itemView.findViewById(R.id.edtRequired);
            itemView.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onItemSelected(item);
                }
            });
            if(isAdmin){
                edtRequired.setEnabled(false);
                edtAvailable.setEnabled(false);
            }else{
                edtRequired.setEnabled(true);
                edtAvailable.setEnabled(true);
            }

        }

        void bindContent(String item) {
            this.item = item;
        }

    }
}
