package com.rmpm.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.rmpm.app.R;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.managers.ReachabilityManager;
import com.rmpm.app.model.CurrentUser;
import com.rmpm.app.model.Location;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.rmpm.app.constants.Constants.kPassword;
import static com.rmpm.app.constants.Constants.kUserName;

public class LoginActivity extends AppCompatActivity {
    private RadioButton radioButtonAdmin;
    private DonutProgress donutProgress;
    private EditText edtUserId;
    private EditText edtPassword;
    private String TAG = getClass().getSimpleName();

    public static Intent getIntent(Context context){
        Intent intent = new Intent(context,LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btnLogin = findViewById(R.id.btnLogin);
        edtUserId = findViewById(R.id.edtUserId);
        edtPassword = findViewById(R.id.edtPassword);
        radioButtonAdmin = findViewById(R.id.rbAdmin);
        donutProgress = findViewById(R.id.donut_progress);
        btnLogin.setOnClickListener(v -> {
            onLogin();
        });


    }

    private void onLogin() {
        String email = Utils.getProperText(edtUserId).trim();
        String password = Utils.getProperText(edtPassword).trim();
        if (!validate())
            return;
        HashMap<String, Object> userMap = new HashMap<>();
        userMap.put(kUserName, email);
        userMap.put(kPassword, password);
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(this, "Signup Failed!", "Sorry, Failed to reach RMPM servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        ModelManager.modelManager().loginUser(userMap, (Constants.Status iStatus) -> {
            CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
            String role = currentUser.getRole();
            boolean isAdmin = role.equalsIgnoreCase("3");
            showProgress(false);
            if (isAdmin)
                startActivity(AdminHomeActivity.getIntent(this, isAdmin));
            else
                startActivity(UserHomeActivity.getIntent(this, isAdmin));
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(LoginActivity.this, "Process Failed!", message);
        });
    }



    private void showProgress(boolean b) {
        if(b){
            donutProgress.setProgress(50);
        }else{
            donutProgress.setProgress(100);
        }
    }

    private boolean validate() {
        boolean isValid = true;
        String email = Utils.getProperText(edtUserId);
        if (email.isEmpty()) {
            isValid = false;
            edtUserId.setError("Field can't be empty!");
            edtUserId.requestFocus();

        }

        String password = Utils.getProperText(edtPassword);
        if (password.isEmpty()) {
            isValid = false;
            edtPassword.setError("Field can't be empty!");
            edtPassword.requestFocus();
        }
        return isValid;
    }
}
