package com.rmpm.app.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.rmpm.app.R;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.model.CurrentUser;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> {
            CurrentUser currentUser = ModelManager.modelManager().getCurrentUser();
            if(currentUser==null ){
                startActivity(LoginActivity.getIntent(SplashActivity.this));
                finish();
            }else{
                String role = currentUser.getRole();
                boolean isAdmin = role.equalsIgnoreCase("3");
                if (isAdmin)
                    startActivity(AdminHomeActivity.getIntent(this, isAdmin));
                else
                    startActivity(UserHomeActivity.getIntent(this, isAdmin));

            }

        }, SPLASH_TIME_OUT);
    }
}
