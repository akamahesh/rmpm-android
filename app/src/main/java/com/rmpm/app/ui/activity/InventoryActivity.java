package com.rmpm.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;
import com.rmpm.app.helper.DividerItemRecyclerDecoration;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.managers.ReachabilityManager;
import com.rmpm.app.model.Location;
import com.rmpm.app.model.Material;
import com.rmpm.app.ui.adapter.InventoryAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.rmpm.app.constants.Constants.kData;
import static com.rmpm.app.ui.activity.AdminHomeActivity.IS_ADMIN;

public class InventoryActivity extends AppCompatActivity {

    boolean isAdmin;
    Location location;
    private String locationID="";
    String TAG = getClass().getName();
    InventoryAdapter inventoryAdapter;
    private List<Material> materialList;

    InventoryAdapter.InventoryAdapterInterface inventoryAdapterInterface = new InventoryAdapter.InventoryAdapterInterface() {
        @Override
        public void onItemSelected(String item) {

        }
    };

    public static Intent getIntent(Context context, boolean isAdmin, Location location) {
        Intent intent = new Intent(context, InventoryActivity.class);
        intent.putExtra(IS_ADMIN,isAdmin);
        intent.putExtra(kData,location);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        isAdmin = getIntent().getBooleanExtra(IS_ADMIN,false);
        location = (Location) getIntent().getSerializableExtra(kData);
        locationID = location.getLineId();
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Inventory Form");
        ImageView ivBack = findViewById(R.id.ivBack);
        //ivBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_24dp));
        ivBack.setOnClickListener(v -> {
              onBackPressed();
        });
        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(v->{
            startActivity(ResultActivity.getIntent(InventoryActivity.this,isAdmin));
        });
        if(isAdmin){
            btnSubmit.setVisibility(View.GONE);
        }else btnSubmit.setVisibility(View.VISIBLE);
        materialList = new ArrayList<>();
        getMeterialList();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
         inventoryAdapter = new InventoryAdapter(this, materialList, inventoryAdapterInterface,isAdmin);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(this, R.drawable.canvas_recycler_divider));
        recyclerView.setAdapter(inventoryAdapter);

        Utils.hideKeyboard(this);
    }



    private void getMeterialList() {

        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(InventoryActivity.this, "Material Failed!", "Sorry, Failed to reach RMPM servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        ModelManager.modelManager().getMaterial(locationID,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Material>> genricResponse) -> {
            showProgress(false);
            CopyOnWriteArrayList<Material> material = ModelManager.modelManager().getCurrentUser().getMaterialList();

            Log.v(TAG, "Response : " + material.size());
            materialList.clear();
            materialList.addAll(material);
            inventoryAdapter.notifyDataSetChanged();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(InventoryActivity.this, "Process Failed!", message);
        });

    }

    private void updateList(){
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(InventoryActivity.this, "Material Failed!", "Sorry, Failed to reach RMPM servers. Please check your network or try again later.");
            return;
        }

        showProgress(true);
        ModelManager.modelManager().getMaterial(locationID,(Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<Material>> genricResponse) -> {
            showProgress(false);
            CopyOnWriteArrayList<Material> material = ModelManager.modelManager().getCurrentUser().getMaterialList();

            Log.v(TAG, "Response : " + material.size());
            materialList.clear();
            materialList.addAll(material);
            inventoryAdapter.notifyDataSetChanged();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(InventoryActivity.this, "Process Failed!", message);
        });
    }

    private void showProgress(boolean b) {
    }


}
