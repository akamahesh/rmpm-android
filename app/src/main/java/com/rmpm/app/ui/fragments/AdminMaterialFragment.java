package com.rmpm.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.rmpm.app.R;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;
import com.rmpm.app.helper.DividerItemRecyclerDecoration;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.managers.ReachabilityManager;
import com.rmpm.app.model.Location;
import com.rmpm.app.model.Material;
import com.rmpm.app.model.MaterialAdmin;
import com.rmpm.app.ui.activity.UserHomeActivity;
import com.rmpm.app.ui.adapter.AdminMaterialAdapter;
import com.rmpm.app.ui.adapter.MaterialAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.rmpm.app.constants.Constants.kData;
import static com.rmpm.app.ui.activity.AdminHomeActivity.IS_ADMIN;


public class AdminMaterialFragment extends Fragment {
    private AdminMaterialAdapter materialAdapter;
    private List<MaterialAdmin> mNewsFeedList;
    private boolean isAdmin;
    private String TAG = getClass().getName();
    Location location;
    public AdminMaterialFragment() {
        // Required empty public constructor
    }


    public static AdminMaterialFragment newInstance(boolean isAdmin, Location item) {
        AdminMaterialFragment fragment = new AdminMaterialFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_ADMIN,isAdmin);
        args.putSerializable(kData,item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isAdmin = getArguments().getBoolean(IS_ADMIN);
            location = (Location) getArguments().getSerializable(kData);
        }
        mNewsFeedList = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
//        EditText edtSearch = view.findViewById(R.id.edtSearch);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        materialAdapter = new AdminMaterialAdapter(getContext(), mNewsFeedList,materialInteraction,isAdmin);
        recyclerView.addItemDecoration(new DividerItemRecyclerDecoration(getContext(), R.drawable.canvas_recycler_divider));
        recyclerView.setAdapter(materialAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isAdmin)
            ((UserHomeActivity) Objects.requireNonNull(getActivity())).updateToolbarTitle("Material List");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(isAdmin){
            getAdminMaterialList();
        }
    }

    private void getAdminMaterialList() {
        if (!ReachabilityManager.getNetworkStatus()) {
            Utils.showAlertDialog(getActivity(), "Material Failed!", "Sorry, Failed to reach RMPM servers. Please check your network or try again later.");
            return;
        }
        showProgress(true);
        ModelManager.modelManager().getAdminMaterial((Constants.Status iStatus, GenricResponse<CopyOnWriteArrayList<MaterialAdmin>> genricResponse) -> {
            showProgress(false);
            CopyOnWriteArrayList<MaterialAdmin> material = ModelManager.modelManager().getCurrentUser().getAdminMaterialList();
            Log.v(TAG, "Response : " + material.size());
            mNewsFeedList.clear();
            mNewsFeedList.addAll(material);
            materialAdapter.notifyDataSetChanged();
        }, (Constants.Status iStatus, String message) -> {
            showProgress(false);
            Utils.showAlertDialog(getActivity(), "Process Failed!", message);
        });

    }

    private void showProgress(boolean b) {
    }

    AdminMaterialAdapter.MaterialInteraction materialInteraction = item -> {
       /* if(isAdmin)
            startActivity(InventoryActivity.getIntent(getContext(),isAdmin));
        else
            startActivity(InventoryActivity.getIntent(getContext(),isAdmin));*/
    };



}
