package com.rmpm.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.model.Material;
import com.rmpm.app.model.MaterialAdmin;

import java.util.List;

public class MaterialAdapter extends RecyclerView.Adapter<MaterialAdapter.MaterialViewHolder> {

    private Context context;
    private List<Material> mList;
    private MaterialInteraction mListener;
    private boolean isAdmin;

    public MaterialAdapter(Context context, List<Material> mList, MaterialInteraction materialInteraction, boolean isAdmin) {
        this.mList = mList;
        this.context = context;
        this.mListener = materialInteraction;
        this.isAdmin = isAdmin;
    }

    @NonNull
    @Override
    public MaterialViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_material_item, parent, false);
        return new MaterialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialViewHolder holder, int position) {
        Material material = mList.get(position);
        holder.bindContent(material);
        String title = material.getMaterial();
        holder.tvTitle.setText(title);
        if (isAdmin) {
            if (position % 2 == 0)
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.green));
            else
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.red));

        }
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface MaterialInteraction {
        void onItemSelected(Material item);
    }

    class MaterialViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private TextView tvDesc;
        private TextView tvMeasurement;
        private Material item;


        public MaterialViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            tvMeasurement = itemView.findViewById(R.id.tvMeasurement);
            itemView.setOnClickListener(v -> {
                if (mListener != null)
                    mListener.onItemSelected(item);
            });
        }

        void bindContent(Material item) {
            this.item = item;
        }
    }
}
