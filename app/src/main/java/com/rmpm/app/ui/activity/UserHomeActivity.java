package com.rmpm.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rmpm.app.R;
import com.rmpm.app.helper.FragmentUtil;
import com.rmpm.app.helper.Utils;
import com.rmpm.app.managers.ModelManager.ModelManager;
import com.rmpm.app.model.Location;
import com.rmpm.app.ui.fragments.LocationFragment;
import com.rmpm.app.ui.fragments.MaterialFragment;

public class UserHomeActivity extends AppCompatActivity {
    public static String IS_ADMIN = "isAdmin";

    private boolean isAdmin;
    private TextView tvTitle;

    public static Intent getIntent(Context context, boolean isAdmin) {
        Intent intent = new Intent(context, UserHomeActivity.class);
        intent.putExtra(IS_ADMIN, isAdmin);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        isAdmin = getIntent().getBooleanExtra(IS_ADMIN, false);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Inventory Report");
        ImageView ivBack = findViewById(R.id.ivBack);
        ImageView ivLogout = findViewById(R.id.ivLogout);
        ivLogout.setVisibility(View.VISIBLE);
        ivLogout.setOnClickListener(v->{
            ModelManager.modelManager().setCurrentUser(null);
            finishAffinity();
            startActivity(LoginActivity.getIntent(UserHomeActivity.this));
        });
        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });
        FragmentUtil.changeFragment(getSupportFragmentManager(), LocationFragment.newInstance(isAdmin),false,true);
        Utils.hideKeyboard(this);

    }

    public void gotoMaterial(Location item) {
        FragmentUtil.changeFragment(getSupportFragmentManager(), MaterialFragment.newInstance(isAdmin,item),true,true);
    }

    public void updateToolbarTitle(String name){
        tvTitle.setText(name);
    }
}
