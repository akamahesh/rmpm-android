package com.rmpm.app.managers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatDelegate;
import android.util.Base64;
import android.util.Log;

import com.rmpm.app.managers.ModelManager.ModelManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class ApplicationManager extends Application {

    //Static Properties
    private static Context _Context;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static Context getContext() {
        return _Context;
    }

    public static ApplicationManager getInstance() {
        return (ApplicationManager) _Context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        _Context = getApplicationContext();
        ModelManager.modelManager();
        printHashKey();
        
    }

    public static void setConnectivityListener(ReachabilityManager.ConnectivityReceiverListener listener) {
        ReachabilityManager._ConnectivityReceiverListener = listener;
    }

    private void printHashKey() {
        try {
            String packageName = getPackageName();
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                    packageName, //set your package name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


}
