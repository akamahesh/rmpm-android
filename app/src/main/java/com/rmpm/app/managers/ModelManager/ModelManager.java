package com.rmpm.app.managers.ModelManager;

import android.util.Log;

import com.google.gson.Gson;
import com.rmpm.app.Libraries.DispatchQueue.DispatchQueue;
import com.rmpm.app.constants.Blocks.Block;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;
import com.rmpm.app.managers.APIManager.APIManager;
import com.rmpm.app.managers.BaseManager.BaseManager;
import com.rmpm.app.model.BaseModel;
import com.rmpm.app.model.CurrentUser;
import com.rmpm.app.model.Location;
import com.rmpm.app.model.Material;
import com.rmpm.app.model.MaterialAdmin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.rmpm.app.managers.APIManager.APIRequestHelper.kLocationList;
import static com.rmpm.app.managers.APIManager.APIRequestHelper.kLogin;


/**
 * Created by akaMahesh on 15/3/18.
 */

/**
 * Singleton class to manage all models, data management and data processing in projects.
 */
public class ModelManager extends BaseManager implements Constants {

    private static final String TAG = ModelManager.class.getSimpleName();
    //Static properties
    private static ModelManager _ModelManger;
    private static String mGenericAuthToken = "";
    //Instance Properties
    private static CurrentUser mCurrentUser = null;
    private static Location locationList =null;
    private Gson gson;
    private DispatchQueue dispatchQueue =
            new DispatchQueue("com.queue.serial.modelmanager", DispatchQueue.QoS.userInitiated);


    /**
     * private constructor
     */
    private ModelManager() {
        this.gson = new Gson();
    }

    /**
     * method to create a threadsafe singleton class instance
     */
    public static synchronized ModelManager modelManager() {
        if (_ModelManger == null) {
            _ModelManger = new ModelManager();

            mCurrentUser = getDataFromPreferences(kCurrentUser, CurrentUser.class);
            Log.i(TAG, "Current User : " + ((mCurrentUser == null) ? null : mCurrentUser.toString()));


        }
        return _ModelManger;
    }

    /**
     * to initialize the singleton object
     */
    public void initializeModelManager() {
        System.out.print("ModelManager object initialized.");
    }

    /**
     * Stores {@link CurrentUser} to the share preferences and synchronize sharedpreferece
     */
    public synchronized void archiveCurrentUser() {
        saveDataIntoPreferences(mCurrentUser, BaseModel.kCurrentUser);
    }

    public synchronized void archiveLocationList() {
        saveDataIntoPreferences(kLineId, BaseModel.kLineId);
    }

    /**
     * getter method for genericAuthToken
     */
    public synchronized String getGenericAuthToken() {
        return mGenericAuthToken;
    }


    public DispatchQueue getDispatchQueue() {
        return dispatchQueue;
    }


    public synchronized CurrentUser getCurrentUser() {
        return mCurrentUser;
    }

    public synchronized void setCurrentUser(CurrentUser o) {
        mCurrentUser = o;
        archiveCurrentUser();
    }




    public void loginUser(HashMap<String, Object> parameters, Block.Status status, Block.Failure failure) {
        dispatchQueue.async(() -> {
            APIManager.APIManager().processFormRequest(kLogin, parameters, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRoot);
                    if(jsonArray.length()==0)
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    CurrentUser currentUser = new CurrentUser(jsonArray.getJSONObject(0));
                    setCurrentUser(currentUser);
                    DispatchQueue.main(() -> status.iStatus(iStatus));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void getLocations( Block.Success<CopyOnWriteArrayList<Location>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            String userID = getCurrentUser().getId();
            String token = getCurrentUser().getAuthorization();
            APIManager.APIManager().processGetRequest(token, userID, (Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRoot);
                    CopyOnWriteArrayList<Location> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
                    if(jsonArray.length()==0)
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    for(int i=0;i<jsonArray.length();i++){
                        Location location = new Location(jsonArray.getJSONObject(i));
                        copyOnWriteArrayList.add(location);
                    }
                    GenricResponse<CopyOnWriteArrayList<Location>> genricResponse1 = new GenricResponse<>(copyOnWriteArrayList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus,genricResponse1));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }

    public void getMaterial(String locID,Block.Success<CopyOnWriteArrayList<Material>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            String userID = getCurrentUser().getId();
            String token = getCurrentUser().getAuthorization();
            APIManager.APIManager().processGetRequestMaterial(token, userID,locID,(Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRoot);
                    CopyOnWriteArrayList<Material> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
                    if(jsonArray.length()==0)
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    for(int i=0;i<jsonArray.length();i++){
                        Material material = new Material(jsonArray.getJSONObject(i));
                        copyOnWriteArrayList.add(material);
                    }
                    getCurrentUser().setMaterialList(copyOnWriteArrayList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<Material>> genricResponse1 = new GenricResponse<>(copyOnWriteArrayList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus,genricResponse1));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }


    public void getAdminMaterial(Block.Success<CopyOnWriteArrayList<MaterialAdmin>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            String userID = getCurrentUser().getId();
            String token = getCurrentUser().getAuthorization();
            APIManager.APIManager().processAdminGetRequestMaterial(token, userID,(Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRoot);
                    CopyOnWriteArrayList<MaterialAdmin> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
                    if(jsonArray.length()==0)
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    for(int i=0;i<jsonArray.length();i++){
                        MaterialAdmin material = new MaterialAdmin(jsonArray.getJSONObject(i));
                        copyOnWriteArrayList.add(material);
                    }
                    getCurrentUser().setAdminMaterialList(copyOnWriteArrayList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<MaterialAdmin>> genricResponse1 = new GenricResponse<>(copyOnWriteArrayList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus,genricResponse1));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }
    public void getUpdateValue(String locID,Block.Success<CopyOnWriteArrayList<Material>> success, Block.Failure failure) {
        dispatchQueue.async(() -> {
            String userID = getCurrentUser().getId();
            String token = getCurrentUser().getAuthorization();
            APIManager.APIManager().processGetRequestMaterial(token, userID,locID,(Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                try {
                    JSONObject jsonObject = genricResponse.getObject();
                    JSONArray jsonArray = jsonObject.getJSONArray(kRoot);
                    CopyOnWriteArrayList<Material> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
                    if(jsonArray.length()==0)
                        DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                    for(int i=0;i<jsonArray.length();i++){
                        Material material = new Material(jsonArray.getJSONObject(i));
                        copyOnWriteArrayList.add(material);
                    }
                    getCurrentUser().setMaterialList(copyOnWriteArrayList);
                    archiveCurrentUser();
                    GenricResponse<CopyOnWriteArrayList<Material>> genricResponse1 = new GenricResponse<>(copyOnWriteArrayList);
                    DispatchQueue.main(() -> success.iSuccess(iStatus,genricResponse1));
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    DispatchQueue.main(() -> failure.iFailure(Status.fail, kMessageInternalInconsistency));
                }
            }, (Status statusFail, String message) -> DispatchQueue.main(() -> failure.iFailure(statusFail, message)));
        });
    }

}
