package com.rmpm.app.managers.APIManager;


import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by akaMahesh on 15/3/18.
 * Copyright to Mobulous Technology Pvt. Ltd.
 */

public interface APIRequestHelper {

    //api key for logout
    String kLogin = "login/";
    String kLocationList = "location/list/";


    /**
     * set api request with api key and corresponding parameters
     *
     * @param APIKey  key of the url
     * @param details details contains request body parameters
     * @param files   if include file will be sent in multipart
     * @return JsonObject ie. response
     */
    @Multipart
    @POST()
    Call<JsonObject> APIRequestWithFile(
            @Url String APIKey,
            @PartMap Map<String, RequestBody> details,
            @Part List<MultipartBody.Part> files
    );


    @GET("location/list/")
    Call<JsonObject> APIRequestGet(@Header("Authorization") String token, @Query("Id") String uid);

    @GET("material/list/")
    Call<JsonObject> APIRequestGetMaterial(@Header("Authorization") String token, @Query("Id") String Id,@Query("location_id") String location_id );

    //http://haridwarmpinfo.com/rmpm/api/materialType/list/?Id=4
    @GET("materialType/list/")
    Call<JsonObject> APIRequestGetAdminMaterial(@Header("Authorization") String token, @Query("Id") String Id );

}
