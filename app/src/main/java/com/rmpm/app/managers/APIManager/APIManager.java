package com.rmpm.app.managers.APIManager;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rmpm.app.constants.Blocks.Block;
import com.rmpm.app.constants.Blocks.GenricResponse;
import com.rmpm.app.constants.Constants;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.rmpm.app.constants.Constants.kEmptyString;
import static com.rmpm.app.constants.Constants.kMessage;
import static com.rmpm.app.constants.Constants.kMessageInternalInconsistency;
import static com.rmpm.app.constants.Constants.kMessageServerNotRespondingError;
import static com.rmpm.app.constants.Constants.kSocketTimeOut;
import static com.rmpm.app.constants.Constants.kStatus;


/**
 * Created by akaMahesh on 15/3/18.
 * Copyright to Mobulous Technology Pvt. Ltd.
 */

public class APIManager {

    public static final String kAPIGooglePlacesBaseURL = "https://maps.googleapis.com/maps/api/";
    private static final String kAPIBaseURL = "http://haridwarmpinfo.com/rmpm/api/";
    private static final String TAG = APIManager.class.getSimpleName();
    private static final int kTimeOut = 25;

    private static final String kContentType = "Content-Type";
    private static final String kContentTypeText = "text/html";
    private static final String kContentTypeJSON = "application/json; charset=utf-8";
    private static final String kContentFormData = "application/x-www-form-urlencoded";
    private static final String kContentTypeMultiPart = "multipart/form-data";
    private static final String kContentTypeImage = "image/jpeg";
    private static final String kDefaultImageName = "photo.jpg";

    private static APIManager _APIManager;
    private Retrofit _Retrofit;
    private APIRequestHelper _APIHelper;

    /**
     * a private constructor to prevent any other class from initiating
     */
    private APIManager() {
    }

    public static synchronized APIManager APIManager() {
        if (_APIManager == null) {
            _APIManager = new APIManager();

            //Todo Need ot remove interceptor in release mode
            //enables logging api requests
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(kTimeOut, TimeUnit.SECONDS)
                    .readTimeout(kTimeOut, TimeUnit.SECONDS)
                    .writeTimeout(kTimeOut, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            _APIManager._Retrofit = new Retrofit.Builder()
                    .baseUrl(kAPIBaseURL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            _APIManager._APIHelper = _APIManager._Retrofit.create(APIRequestHelper.class);
        }
        return _APIManager;
    }

    /**
     * converts parameter into @{@link retrofit2.http.Multipart} multipart/form-data
     *
     * @param parameter map which contains request body
     * @return @{@link RequestBody} for multipart request : multipart/form-data
     */
    private RequestBody requestBody(String parameter) {
        return RequestBody.create(MediaType.parse(kContentTypeMultiPart), parameter);
    }

    /**
     * create api request with apiKEY and corresponding parameters. This method will work for all cases.
     * 1. To upload multiple file to server
     * 2. To get data from server in multipart format.
     *
     * @param APIKey     contains api key
     * @param parameters to be include as body to the request
     * @return return a @{@link retrofit2.http.Multipart} request with the type of form-data
     */
    private Call<JsonObject> getAPIRequest(String APIKey, HashMap<String, Object> parameters) {
        //process the parameters as per the File and details. If object is of File type then it create MutilpartBody.Part and store it in fileList else object will be store in detailMap. detailMap and fileList will be use to create APIRequest.
        List<MultipartBody.Part> fileList = new ArrayList<>();
        HashMap<String, RequestBody> detailMap = new HashMap<>();
        for (String key : parameters.keySet()) {
            if (key != null && !key.equals(kEmptyString)) {
                Object value = parameters.get(key);
                if (value.getClass() == File.class) {
                    File file = new File(value.toString());
                    if (file.exists()) {
                        //create RequestBody instance from file
                        RequestBody requestFile = RequestBody.create(MediaType.parse(kContentTypeImage), file);
                        MultipartBody.Part body = MultipartBody.Part.createFormData(key, kDefaultImageName, requestFile);

                        fileList.add(body);
                    }
                } else if (value.getClass() == HashMap.class) {
                    //Initialize Builder (not RequestBody)
                    FormBody.Builder builder = new FormBody.Builder();
                    // add params to builder
                    for (Map.Entry<String, Object> entry : ((HashMap<String, Object>) value).entrySet()) {
                        builder.add(entry.getKey(), entry.getValue().toString());
                    }
                    //Create RequestBody
                    RequestBody formBody = builder.build();
                    detailMap.put(key, formBody);
                } else {
                    detailMap.put(key, requestBody(parameters.get(key).toString()));

                }
            }
        }
        return _APIHelper.APIRequestWithFile(APIKey, detailMap, fileList);
    }

    /**
     * checkStatus is responsible to check whether api return the desired result or not by check
     * the api status. If status is success then it returns JSONObject other wise return a
     * suitable message.
     *
     * @param response json response result
     * @param success  Block to be executed for success condition
     * @param failure  Block to be executed for success condition
     */
    private void checkStatus(JsonObject response, Block.Success<JSONObject> success, Block.Failure failure) {
        try {
            JSONObject jsonResposne = new JSONObject(response.toString());
            GenricResponse<JSONObject> genricResponse = new GenricResponse<>(jsonResposne);
            Log.v(this.getClass().getSimpleName(), "APIResponse :" + jsonResposne.toString(4));

            Constants.HTTPStatus status = Constants.HTTPStatus.getStatus(jsonResposne.getString(kStatus));
            String message = jsonResposne.getString(kMessage);
            if (status == Constants.HTTPStatus.success) {
                success.iSuccess(Constants.Status.success, genricResponse);
            } else {
                failure.iFailure(Constants.Status.fail, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            failure.iFailure(Constants.Status.fail, kMessageInternalInconsistency);
        }
    }

    /**
     * base method to get data from server
     *
     * @param request a retrofit {@link Call} containing parameters
     * @param success Block to be executed for success condition
     * @param failure Block to be executed for failure condition
     */
    private void apiRequestWithAPI(Call<JsonObject> request, Block.Success<JSONObject> success, Block.Failure failure) {
        try {
            JsonObject response = request.execute().body();
            if (response != null) {
                checkStatus(response, (Constants.Status iStatus, GenricResponse<JSONObject> genricResponse) -> {
                    //if success, it return JSONObject if fail, it return message
                    if (iStatus == Constants.Status.success) {
                        success.iSuccess(Constants.Status.success, genricResponse);
                    }
                }, (Constants.Status iStatus, String message) -> {
                    //If failure occurred.
                    failure.iFailure(Constants.Status.fail, message);
                });
            } else {
                failure.iFailure(Constants.Status.fail, kMessageServerNotRespondingError);
            }
        } catch (Exception e) {
            e.printStackTrace();
            failure.iFailure(Constants.Status.fail, kSocketTimeOut);
        }
    }


    /**
     * method to make convert the call into a raw one with type application/json
     *
     * @param APIKey     that to be called
     * @param parameters to be include as body to the request
     * @return a retrofit {@link Call} with content type application/json; charset=utf-8
     */
    public void processFormRequest(String APIKey, HashMap<String, Object> parameters,
                                   Block.Success<JSONObject> success, Block.Failure failure) {
        final Call<JsonObject> request = getAPIRequest(APIKey, parameters);
        apiRequestWithAPI(request, success, failure);
    }


    public void processGetRequest(String token,String uid,
                                  Block.Success<JSONObject> success, Block.Failure failure) {
        final Call<JsonObject> request = _APIHelper.APIRequestGet(token,uid);
        apiRequestWithAPI(request, success, failure);
    }

    public void processGetRequestMaterial(String token,String uid,String locationID,
                                  Block.Success<JSONObject> success, Block.Failure failure) {
        final Call<JsonObject> request = _APIHelper.APIRequestGetMaterial(token,uid,locationID);
        apiRequestWithAPI(request, success, failure);
    }
    public void processAdminGetRequestMaterial(String token,String uid,
                                  Block.Success<JSONObject> success, Block.Failure failure) {
        final Call<JsonObject> request = _APIHelper.APIRequestGetAdminMaterial(token,uid);
        apiRequestWithAPI(request, success, failure);
    }

}
